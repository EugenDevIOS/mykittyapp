import Foundation

protocol AppApiClientProtocol: AnyObject {
    func getCats(breedsIds: [String], completion: @escaping ([Cat]?) -> Void)
    func getBreedsList(completion: @escaping ([Breed]?) -> Void)
}

class AppApiClient {

    private enum Constant {
        static let apiKey = "d7984110-eea6-49e5-a2cd-9fb0e1217d3a"
        static let uuid = UUID().uuidString
    }

    private let urlSession = URLSession.shared
    private var dataTask: URLSessionDataTask?

    enum AppApiClientEndpoint {
        private static let apiHost: String = "https://api.thecatapi.com/v1"

        case allCat
        case breeds
        case categories
        case favourites
        case upload

        func urlString() -> String {
            switch self {
            case .allCat:
                return "\(AppApiClientEndpoint.apiHost)/images/search"
            case .breeds:
                return "\(AppApiClientEndpoint.apiHost)/breeds"
            case .categories:
                return "\(AppApiClientEndpoint.apiHost)/categories"
            case .favourites:
                return "\(AppApiClientEndpoint.apiHost)/favourites"
            case .upload:
                return "\(AppApiClientEndpoint.apiHost)/images/upload"
            }
        }

    }

}

extension AppApiClient: AppApiClientProtocol {

    func getCats(breedsIds: [String], completion: @escaping ([Cat]?) -> Void) {
        fetchCats(breedsIds: breedsIds, completion: { result in
            switch result {
            case .success(let result):
                completion(result)
            case .failure(_):
                completion(nil)
            }
        })
    }

    func getBreedsList(completion: @escaping ([Breed]?) -> Void) {
        fetchBreeds { result in
            switch result {
            case .success(let result):
                completion(result)
            case .failure(_):
                completion(nil)
            }
        }
    }

}

private extension AppApiClient {

    private func fetchCats(breedsIds: [String], completion: @escaping (Result<[Cat], Error>) -> Void) {
        guard var components = URLComponents(string: AppApiClientEndpoint.allCat.urlString()) else {
            return
        }

        var queryParameters: [String: String] = [:]
        queryParameters["limit"] = "5"
        queryParameters["size"] = "small"

        components.queryItems = queryParameters.map({ (key, value) in
            URLQueryItem(name: key, value: value)
        })

        if !breedsIds.isEmpty {
            breedsIds.forEach({ (identifier) in
                let queryItem = URLQueryItem(name: "breed_ids", value: identifier)
                components.queryItems?.append(queryItem)
            })
        }

        guard let url = components.url else {
            return
        }

        var request = URLRequest(url: url)

        request.httpMethod = "GET"
        request.setValue(Constant.apiKey, forHTTPHeaderField: "x-api-key")

        let oldDataTask = dataTask
        dataTask = nil
        oldDataTask?.cancel()

        dataTask = urlSession.dataTask(with: request) { (data, _, error) in
            if let actualError = error {
                completion(.failure(actualError))
                return
            }
            if let actualData = data {
                do {
                    let decoder = JSONDecoder()
                    let cats = try decoder.decode([Cat].self, from: actualData)
                    completion(.success(cats))
                } catch let error {
                    completion(.failure(error))
                }
            }
        }
        dataTask?.resume()
    }

    private func fetchBreeds(completion: @escaping (Result<[Breed], Error>) -> Void) {
        guard var components = URLComponents(string: AppApiClientEndpoint.breeds.urlString()) else {
            return
        }
        let queryParameters: [String: String] = [:]
//        queryParameters["page"] = "1"
//        queryParameters["limit"] = "10"

        components.queryItems = queryParameters.map({ (key, value) in
            URLQueryItem(name: key, value: value)
        })

        guard let url = components.url else {
            return
        }

        var request = URLRequest(url: url)

        request.httpMethod = "GET"
        request.setValue(Constant.apiKey, forHTTPHeaderField: "x-api-key")

        let oldDataTask = dataTask
        dataTask = nil
        oldDataTask?.cancel()

        dataTask = urlSession.dataTask(with: request) { (data, _, error) in
            if let actualError = error {
                completion(.failure(actualError))
                return
            }
            if let actualData = data {
                do {
                    let decoder = JSONDecoder()
                    let breeds = try decoder.decode([Breed].self, from: actualData)
                    completion(.success(breeds))
                } catch let error {
                    completion(.failure(error))
                }
            }
        }
        dataTask?.resume()
    }

}
