import Foundation

struct Cat: Codable {

    private enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case imageUrlString = "url"
        case breed = "name"
    }

    let identifier: String
    var breed: String?
    var imageUrl: URL? {
        return URL(string: imageUrlString)
    }

    private let imageUrlString: String

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        identifier = try values.decode(String.self, forKey: .identifier)
        imageUrlString = try values.decode(String.self, forKey: .imageUrlString)
        breed = try? values.decode(String.self, forKey: .breed)
    }

}
