import Foundation

struct Breed: Codable {

    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case name
    }
    let name: String
    let identifier: String

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        identifier = try values.decode(String.self, forKey: .identifier)
    }

}

extension Breed: Equatable {
    static func == (lhs: Breed, rhs: Breed) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}
