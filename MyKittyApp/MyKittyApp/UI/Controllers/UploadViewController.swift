import UIKit

class UploadViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

// MARK: - BaseViewControllerProtocol

extension UploadViewController: BaseViewControllerProtocol {

    typealias OptionsType = Void

    static func instantiate(_ options: Void) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = unwrap({ storyboard.instantiateViewController(withIdentifier: "UploadViewController") as? UploadViewController })
        return viewController
    }

}
