import Foundation
import UIKit

class SearchAnimator: NSObject, UIViewControllerAnimatedTransitioning {

    static let duration: TimeInterval = 1.25

    private let originFrame: CGRect
    private let snapshotCell: UIView

    init(originFrame: CGRect, snapshotCell: UIView) {
        self.originFrame = originFrame
        self.snapshotCell = snapshotCell
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return Self.duration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        guard let toViewController = transitionContext.viewController(forKey: .to) else {
            transitionContext.completeTransition(false)
            return
        }

        containerView.addSubview(toViewController.view)
        containerView.addSubview(snapshotCell)
        toViewController.view.isHidden = true

        snapshotCell.frame = originFrame

        let finalFrame = transitionContext.finalFrame(for: toViewController)

        UIView.animateKeyframes(withDuration: Self.duration, delay: 0, options: .calculationModeCubic, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1) {
                self.snapshotCell.frame = finalFrame
            }
        }, completion: { _ in
            toViewController.view.isHidden = false
            self.snapshotCell.removeFromSuperview()

            transitionContext.completeTransition(true)
        })
    }
}
