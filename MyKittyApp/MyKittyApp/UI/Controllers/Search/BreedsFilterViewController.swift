import UIKit

struct BreedsFilterViewControllerOptions {
    let completion: (([Breed]) -> Void)?
}

class BreedsFilterViewController: BaseViewController {

    @IBOutlet private var tableView: UITableView!

    private var breeds: [Breed] = []
    private var selectedBreeds: [String: Breed] = [:]
    private var apiClient: AppApiClientProtocol?

    private var completion: (([Breed]) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        apiClient = AppApiClient()
        fetchBreeds()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let selectedBreeds = Array(selectedBreeds.values)
        completion?(selectedBreeds)
    }

}

extension BreedsFilterViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return breeds.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: BreedsTableViewCell.identifier, for: indexPath) as? BreedsTableViewCell else {
            return UITableViewCell()
        }
        let breed = breeds[indexPath.row]
        cell.setupCell(breed: breed)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let cell = tableView.cellForRow(at: indexPath) as? BreedsTableViewCell else {
            return
        }
        let selectedBreed = breeds[indexPath.row]
        if cell.isSelectedBreed {
            selectedBreeds.removeValue(forKey: selectedBreed.name)
            cell.isSelectedBreed = false
        } else {
            selectedBreeds[selectedBreed.name] = selectedBreed
            cell.isSelectedBreed = true
        }

    }

}

extension BreedsFilterViewController: BaseViewControllerProtocol {

    typealias OptionsType = BreedsFilterViewControllerOptions

    static func instantiate(_ options: OptionsType) -> UIViewController {
        let storyboard = UIStoryboard(name: "Search", bundle: nil)
        let viewController = unwrap({ storyboard.instantiateViewController(withIdentifier: "BreedsFilterViewController") as? BreedsFilterViewController })
        viewController.completion = options.completion
        return viewController
    }

}

private extension BreedsFilterViewController {

    private func fetchBreeds() {
        apiClient?.getBreedsList(completion: { [weak self] (breeds) in
            guard let actualSelf = self else {
                return
            }
            if let actualBreeds = breeds {
                actualSelf.breeds = actualBreeds
                DispatchQueue.main.async {
                    actualSelf.tableView.reloadData()
                }
            }
        })
    }

}
