import UIKit

class SearchCollectionViewCell: UICollectionViewCell {

    @IBOutlet private var catImageView: UIImageView!
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!

    static let identifier: String = "searchCollectionViewCell"

    override func prepareForReuse() {
        super.prepareForReuse()
        catImageView.image = nil
        activityIndicator.stopAnimating()
    }

    func setup(_ cat: Cat) {
        activityIndicator.startAnimating()
        catImageView.loadImage(url: cat.imageUrl, completion: { [weak self] in
            self?.activityIndicator.stopAnimating()
        })
    }

}
