import UIKit

class BreedsTableViewCell: UITableViewCell {

    @IBOutlet private var checkBoxImageView: UIImageView!
    @IBOutlet private var breedNameLabel: UILabel!

    static let identifier: String = "breedsTableViewCell"

    var isSelectedBreed: Bool = false {
        didSet {
            checkBoxImageView.image = isSelectedBreed ? UIImage(named: "selected_checkbox") : UIImage(named: "empty_checkbox")
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupCell(breed: Breed) {
        checkBoxImageView.image = UIImage(named: "empty_checkbox")
        breedNameLabel.text = breed.name
    }

}
