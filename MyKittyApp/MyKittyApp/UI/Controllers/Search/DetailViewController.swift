import UIKit

struct DetailViewControllerOptions {
    let cat: Cat
}

class DetailViewController: BaseViewController {

    @IBOutlet private var catImageView: UIImageView!
    @IBOutlet private var breedLabel: UILabel!

    private var cat: Cat?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true

        catImageView.loadImage(url: cat?.imageUrl, completion: nil)
        breedLabel.text = cat?.breed ?? "not found"
    }

    @IBAction private func likeButtonTapped(_ sender: Any) {
    }

    @IBAction func closeButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}

extension DetailViewController: BaseViewControllerProtocol {

    typealias OptionsType = DetailViewControllerOptions

    static func instantiate(_ options: OptionsType) -> UIViewController {
        let storyboard = UIStoryboard(name: "Search", bundle: nil)
        let viewController = unwrap({ storyboard.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController })
        viewController.cat = options.cat
        return viewController
    }

}
