import UIKit

class SearchViewController: BaseViewController {

    @IBOutlet private var collectionView: UICollectionView!
    @IBOutlet private var breedsButton: UIButton!

    private var apiClient: AppApiClientProtocol?

    private var selectedBreeds: [Breed] = []

    private var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reloadCats(sender:)), for: .valueChanged)
        return refreshControl
    }()

    private var cats: [Cat] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        apiClient = AppApiClient()
        collectionView.refreshControl = refreshControl

        loadCats()
    }

    @IBAction private func breedsButtonTapped(_ sender: Any) {
        let completion: ([Breed]) -> Void = { [weak self] (result) in
            guard !result.isEmpty else {
                return
            }
            let breedsString = result.compactMap({ $0.name }).joined(separator: ", ")
            let breedsIds = result.compactMap({ $0.identifier })
            self?.selectedBreeds = result
            self?.breedsButton.setTitle("Breeds: \(breedsString)", for: .normal)
            self?.loadCats(breedsIds: breedsIds, completion: nil)
        }
        let breedsViewController = BreedsFilterViewController.instantiate(BreedsFilterViewControllerOptions(completion: completion))
        present(breedsViewController, animated: true, completion: nil)
    }

}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout

extension SearchViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cats.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchCollectionViewCell.identifier, for: indexPath) as? SearchCollectionViewCell else {
            return UICollectionViewCell()
        }
        let cat = cats[indexPath.row]
        cell.setup(cat)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.width)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = cats[indexPath.item]
        let detailViewController = DetailViewController.instantiate(DetailViewControllerOptions(cat: item))
        navigationController?.pushViewController(detailViewController, animated: true)
    }

}

// MARK: - BaseViewControllerProtocol

extension SearchViewController: BaseViewControllerProtocol {

    typealias OptionsType = Void

    static func instantiate(_ options: Void) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = unwrap({ storyboard.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController })
        return viewController
    }

}

// MARK: - Private

private extension SearchViewController {

    private func loadCats(breedsIds: [String] = [], completion: (() -> Void)? = nil) {
        apiClient?.getCats(breedsIds: breedsIds, completion: { [weak self] cats in
            guard let actualSelf = self else {
                return
            }
            if let actualCats = cats {
                actualSelf.cats = actualCats
                DispatchQueue.main.async {
                    completion?()
                    actualSelf.collectionView.reloadData()
                }
            }
        })
    }

    @objc private func reloadCats(sender: UIRefreshControl) {
        loadCats(completion: {
            sender.endRefreshing()
        })
    }

}
