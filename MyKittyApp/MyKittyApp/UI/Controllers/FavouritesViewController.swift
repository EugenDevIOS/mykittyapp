import UIKit

class FavouritesViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

// MARK: - BaseViewControllerProtocol

extension FavouritesViewController: BaseViewControllerProtocol {

    typealias OptionsType = Void

    static func instantiate(_ options: Void) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = unwrap({ storyboard.instantiateViewController(withIdentifier: "FavouritesViewController") as? FavouritesViewController })
        return viewController
    }

}
