import UIKit

class HeartBeatButton: UIButton {

    private let cornerRadius: CGFloat = 12.0
    private var isLiked: Bool = false
    private let duration: TimeInterval = 1.0

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    // MARK: - Overrides

    public override func layoutSubviews() {
        super.layoutSubviews()
        titleLabel?.frame = bounds
        imageView?.frame = bounds
    }

    public override func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title, for: state)
    }

    private func commonInit() {
        addTarget(self, action: #selector(buttonDidTap(_:)), for: .touchUpInside)
        layer.masksToBounds = true
        layer.cornerRadius = cornerRadius

        setTitle("", for: .normal)

        setImage(UIImage(named: "heart_unselected"), for: .normal)
        setImage(UIImage(named: "heart_selected"), for: .selected)
    }

    @objc private func buttonDidTap(_ sender: UIButton) {
        isSelected.toggle()
        let animation = CASpringAnimation(keyPath: "transform.scale")
        animation.duration = duration
        animation.damping = 2.0
        animation.fromValue = 1.2
        animation.toValue = 1.0
        animation.isRemovedOnCompletion = true

        layer.add(animation, forKey: nil)
    }

}
