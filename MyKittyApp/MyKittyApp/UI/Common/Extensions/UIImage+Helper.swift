import UIKit

extension UIImageView {

    func loadImage(url: URL?, completion: (() -> Void)?) {
        guard let path = url else {
            return
        }
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: path),
                  let image = UIImage(data: imageData) else {
                      return
                  }
            DispatchQueue.main.async {
                self.image = image
                completion?()
            }
        }
    }

}
