import UIKit

protocol BaseViewControllerProtocol: AnyObject {
    associatedtype OptionsType
    static func instantiate(_ options: OptionsType) -> UIViewController
}

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

extension BaseViewControllerProtocol {

    public static func unwrap<T>(_ block: () -> T?) -> T {
        guard let result = block() else {
            fatalError("Can not instantiate view controller from storyboard")
        }
        return result
    }

}
